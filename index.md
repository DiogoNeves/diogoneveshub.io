---
layout: page
---

Diogo (see :point_up:) is a **Software Engineer @ LOST**.

He has helped improving **communications on public transport**, worked on
(BAFTA award winning) **Little Big Planet PSP, Killzone Mercenary** and
tried to dominate the world twice.
He is currently working on the **next generation of Music applications**.

Diogo spends much of his free time with **wife and daughter** in happy Cambridge
(Mostly talking about themselves in the third person, which is impressive
with a newborn daughter!), reading and learning more about Computer Science and
Artificial Intelligence.

### Work

* *(currently)* Developing the next gen of Music applications @
[LOST](http://lostmusic.london/){:target="_blank"}

* Fitting bits in small places @ [Icomera](http://www.icomera.com/){:target="_blank"}
* Developing Games, Concepts and Tools @
[Guerrilla Cambridge](http://www.worldwidestudios.net/cambridge){:target="_blank"}
(ex Sony Cambridge)
* Developing Facebook applications @ [Wildbunny](http://wildbunny.co.uk/){:target="_blank"}
(Founder)
* Smashing bits, developing more games and general crazy person
@ [Vortix Games Studios](http://blog.vortixgames.com/){:target="_blank"} (Founder)
* Researching Motion Capture data formats for artistic body motion @
[INESC-ID](http://www.inesc-id.pt/){:target="_blank"}

-----

### Curiosities

* Interested in extending the [Human Capacity](http://en.wikipedia.org/wiki/Capacity_development#Defining_Capacity_Development){:target="_blank"}
with technological solutions
* Won a [BAFTA as part of the Little Big Planet Team](http://www.bafta.org/games/awards/2010-winners-nominees,2475,BA.html){:target="_blank"}
* Can't burp
* Speaks English and Portuguese
* Loves to visit busy [cities](http://en.wikipedia.org/wiki/London){:target="_blank"}
* Doesn't like bullet point lists

### My Current Stack

* Atom, PyCharm, Android Studio,
 Vim ([my config](https://github.com/DiogoNeves/vim_config){:target="_blank"}), Git
* Python 2.7 (Yes, I want to upgrade...)  
* Java (on Android)  
* [Books](https://www.goodreads.com/DiogoSnows), Udacity, EdX, Coursera  
* Other stuff not worth mentioning here

-----

### Contact me

[{{site.footer-links.email}}](mailto:{{site.footer-links.email}})
or find me at some local
[meetup](http://www.meetup.com/members/11995734/){:target="_blank"}
